clear all;

# Ucitavanje slike 
obj = videoinput ("v4l2", "/dev/video1");
set(obj,"VideoFormat","YUYV")
start(obj)
img = getsnapshot(obj);
tmp = cat (3, img.Y, kron(img.Cb, [1 1]), kron(img.Cr, [1 1]));
pkg load image
rgb = ycbcr2rgb (tmp, "709");
stop(obj);

hsv = rgb2hsv(rgb);
hsv_val = hsv(:,:,3)*255;
crop = (hsv_val);
crop = crop(50:350,:);
#izvod = abs(diff(crop));
#[h,b] = hist(izvod,255);
h = hist(crop,255);
s = h';
#plot(sum(s));
#plot(izvod);
suma = sum(s);
br_pix = 0;
br_svih = 301*640;
for i = 1:255;
  br_pix = br_pix+suma(i);
  if(br_pix >= br_svih*0.992) 
  thresh = i
  break;
  end;
end;
figure;
imshow(crop>thresh);
slika = crop>thresh;
slika = medfilt2(slika,[4,4]);
se = strel ("square", 4);
seq = getsequence(se);
i_dil = imdilate(slika,se);
prop = regionprops(i_dil);
area_size = 0;
imp_body = 0;
br_tela = length(prop);
for i=1:br_tela
  temp = prop(i).Area;
  if(temp>area_size) 
  area_size = temp;
  imp_body = i;
  end;
  end;
centroid = prop(imp_body).Centroid;